from django.contrib import admin
from .models import *
# Register your models here.
admin.site.register(Locations)
admin.site.register(OTP)
admin.site.register(Trucks)
admin.site.register(Details)
admin.site.register(Bookings)