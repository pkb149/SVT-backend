from django.contrib.auth.models import User
from rest_framework import serializers
from .models import OTP
from rest_framework.compat import authenticate
from django.utils.translation import ugettext_lazy as _

class RegisterSerializer(serializers.ModelSerializer):
    #username = serializers.CharField(label=_("Username"))
    password = serializers.CharField(
        label=_("Password"),
        style={'input_type': 'password'},
        trim_whitespace=False
    )
    class Meta:
        model = User
        fields = ('first_name','id','username', 'password','last_name','is_active')
        extra_kwargs = {"password":
                            {"write_only": True},
                        "groups":
                            {"write_only": True},
                        }
        read_only_fields = ('id',)
    def create(self, validated_data):
        # user = User.objects.create_user(username=validated_data['username'],password=validated_data['password'],first_name=validated_data['first_name'],last_name=validated_data['last_name'],is_active=False)
        pass

class OtpSerializer(serializers.ModelSerializer):
    class Meta:
        model=OTP
        fields=("phone","otp","created_date")


class UserSerializer(serializers.Serializer):
    username = serializers.CharField(label=_("Username"))
    password = serializers.CharField(
        label=_("Password"),
        style={'input_type': 'password'},
        trim_whitespace=False
    )
    class Meta:
        model = User
        fields = ('username', 'password',)
    def validate(self, attrs):
        username = attrs.get('username')
        password = attrs.get('password')

        if username and password:
            user = authenticate(request=self.context.get('request'),
                                username=username, password=password)

            # The authenticate call simply returns None for is_active=False
            # users. (Assuming the default ModelBackend authentication
            # backend.)
            if not user:
                msg = _('Unable to log in with provided credentials.')
                raise serializers.ValidationError(msg, code='authorization')
        else:
            msg = _('Must include "username" and "password".')
            raise serializers.ValidationError(msg, code='authorization')

        attrs['user'] = user
        return attrs