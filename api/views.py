from django.contrib.auth.models import User,Group
from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated

from .models import OTP
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework.views import APIView
import django
from api.serializers import RegisterSerializer,UserSerializer,OtpSerializer
from rest_framework import parsers, renderers
import urllib,pyotp
from django.utils import timezone
authkey = "182547AyPIIazGn5a013451"  # Your authentication key.
# Create your views here.

class Register(generics.ListCreateAPIView):
    queryset = User.objects.all()
    serializer_class = RegisterSerializer

    def post(self, request, *args, **kwargs):
        print(request.data)
        if User.objects.filter(username=self.request.data['username']).exists():
            return Response({"user": ["Already exists."]}, status=status.HTTP_400_BAD_REQUEST)
        else:
            otp = pyotp.TOTP('base32secret3232').now()
            print(otp)
            mobiles = self.request.data['username']  # Multiple mobiles numbers separated by comma.
            message = "your otp is {}. Keep OTP with you.".format(otp)  # Your message to send.
            sender = "iPOLAA"  # Sender ID,While using route4 sender id should be 6 characters long.
            route = "route4"  # Define route
            values = {
                'authkey': authkey,
                'mobiles': mobiles,
                'message': message,
                'sender': sender,
                'route': route
            }
            url = "http://api.msg91.com/api/sendhttp.php"  # API URL
            postdata = urllib.parse.urlencode(values).encode("utf-8")  # URL encoding the data here.
            response = urllib.request.urlopen(url, postdata)
            output = response.read()  # Get Response
            print(output)  # Print Response
            OTP.objects.create(phone=self.request.data['username'], otp=otp)

            #auth_token = Token.objects.create(user_id=response.data['id'])
            #return Response({'token': auth_token.key, "group":response.data['last_name']})
            return Response({'status': "otp sent"})

class Status(APIView):
    def get(self, request, *args, **kwargs):
        return Response({"success": True,"django version":django.get_version()})


class ValidateOtp(APIView):
    # queryset = OTP.objects.all()
    # serializer_class = OtpSerializer
    def post(self, request, *args, **kwargs):
        OtpObj=OTP.objects.filter(phone=self.request.data['username']).order_by('-created_date')[:1]
        if request.data['otp']== OtpObj.values()[0]['otp'] and (timezone.now()-OtpObj.values()[0]['created_date']).total_seconds()<180:
            user = User.objects.create_user(username=self.request.data['username'],password=self.request.data['password'],first_name=self.request.data['first_name'],last_name=self.request.data['last_name'],is_active=True)
            return Response({'status': "success"})
        else:
            return Response({"otp": ["invalid otp, please try again."]}, status=status.HTTP_400_BAD_REQUEST)


class ResetPassword(APIView):
    def post(self, request, *args, **kwargs):
        print(request.data)
        OtpObj=OTP.objects.filter(phone=self.request.data['username']).order_by('-created_date')[:1]
        if request.data['otp']== OtpObj.values()[0]['otp'] and (timezone.now()-OtpObj.values()[0]['created_date']).total_seconds()<180:
            user=User.objects.get(username=self.request.data['username'])
            user.set_password(request.data['password'])
            user.save()
            return Response({'status': "success"})
        else:
            return Response({"otp": ["invalid otp, please try again."]}, status=status.HTTP_400_BAD_REQUEST)


class SendOtpToResetPassword(generics.ListCreateAPIView):
    queryset = User.objects.all()
    serializer_class = RegisterSerializer

    def post(self, request, *args, **kwargs):
        print(request.data)
        if User.objects.filter(username=self.request.data['username']).exists():
            otp = pyotp.TOTP('base32secret3232').now()
            print(otp)
            mobiles = self.request.data['username']  # Multiple mobiles numbers separated by comma.
            message = "your otp is {}. Keep OTP with you.".format(otp)  # Your message to send.
            sender = "iPOLAA"  # Sender ID,While using route4 sender id should be 6 characters long.
            route = "route4"  # Define route
            values = {
                'authkey': authkey,
                'mobiles': mobiles,
                'message': message,
                'sender': sender,
                'route': route
            }
            url = "http://api.msg91.com/api/sendhttp.php"  # API URL
            postdata = urllib.parse.urlencode(values).encode("utf-8")  # URL encoding the data here.
            response = urllib.request.urlopen(url, postdata)
            output = response.read()  # Get Response
            OTP.objects.create(phone=self.request.data['username'], otp=otp)
            return Response({'status': "otp sent"})
        else:
            return Response({'user': "Not Registered"}, status=status.HTTP_400_BAD_REQUEST)


class ObtainAuthToken(APIView):
    throttle_classes = ()
    permission_classes = ()
    parser_classes = (parsers.FormParser, parsers.MultiPartParser, parsers.JSONParser,)
    renderer_classes = (renderers.JSONRenderer,)
    serializer_class = UserSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({'token': token.key, "group":str(user.last_name)})


class Search(APIView):
    permission_classes = (IsAuthenticated,)
    def post(self,request):
        print(request.data)
        return Response({'status':'success'})

