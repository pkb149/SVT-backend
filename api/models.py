from django.contrib.auth.models import User
from django.db import models

class OTP(models.Model):
    phone = models.CharField(max_length=10)
    otp = models.CharField(max_length=4)
    created_date=models.DateTimeField(auto_now_add=True)

class Locations(models.Model):
    location=models.CharField(max_length=20)

class Trucks(models.Model):
    truck_number = models.CharField(max_length=16)
    driver_id = models.ForeignKey(User,on_delete=models.CASCADE,related_name='driver_id')
    owner_id = models.ForeignKey(User, on_delete=models.CASCADE,related_name='owner_id')
    truck_base_location=models.ForeignKey(Locations,on_delete=models.CASCADE)

class Details(models.Model):
    end_user_mobile=models.CharField(max_length=10)
    from_address=models.CharField(max_length=100)
    to_address=models.CharField(max_length=100)

class Bookings(models.Model):
    start_time=models.DateTimeField()
    end_time=models.DateTimeField()
    from_location=models.ForeignKey(Locations,on_delete=models.CASCADE,related_name="from_location")
    to_location = models.ForeignKey(Locations, on_delete=models.CASCADE, related_name="to_location")
    agent_id=models.ForeignKey(User,on_delete=models.CASCADE,related_name='agent_id')
    truck_id=models.ForeignKey(Trucks,on_delete=models.CASCADE,related_name='truck_id')
    details_id=models.ForeignKey(Details,on_delete=models.CASCADE)
