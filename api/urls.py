
from django.conf.urls import url
from . import views as CustomViews
from rest_framework.authtoken import views

urlpatterns = [
    url(r'^v0/login/$', CustomViews.ObtainAuthToken.as_view(),name='login'),
    url(r'^v0/register/$', CustomViews.Register.as_view(),name='register'),
    url(r'^v0/status/$', CustomViews.Status.as_view(),name='status'),
    url(r'^v0/validate_otp/$', CustomViews.ValidateOtp.as_view(),name='status'),
    url(r'^v0/reset_password/$', CustomViews.ResetPassword.as_view(),name='status'),
    url(r'^v0/send_otp_to_reset_password/$', CustomViews.SendOtpToResetPassword.as_view(),name='status'),
    url(r'^v0/search/$', CustomViews.Search.as_view(),name='status'),
]